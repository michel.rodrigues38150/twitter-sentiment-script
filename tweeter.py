import tweepy
import re
import pandas as pd
import matplotlib.pyplot as plt
from   textblob import TextBlob

#Credentials
consumer_key="YOUR_KEY"
consumer_secret="YOUR_SECRET_KEY"
key="KEY"
secret="SECRET"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(key, secret)
api = tweepy.API(auth)


def remove_url(txt):
    return " ".join(re.sub("([^0-9A-Za-z \t])|(\w+://\S+)", "", txt).split())

# Create a custom search term and define the number of tweets
search_term = "Apple"

tweets = tweepy.Cursor(api.search,
                   q= search_term + ' -filter:retweets',
                   lang="fr",
                   since='2019-03-01').items(1000)

tweets_no_urls = [remove_url(tweet.text) for tweet in tweets]

# Create textblob objects of the tweets
sentiment_objects = [TextBlob(tweet) for tweet in tweets_no_urls]
sentiment_objects[0].polarity, sentiment_objects[0]
# Create list of polarity valuesx and tweet text
sentiment_values = [[tweet.sentiment.polarity, str(tweet)] for tweet in sentiment_objects]
sentiment_values[0]
# Create dataframe containing the polarity value and tweet text
sentiment_df = pd.DataFrame(sentiment_values, columns=["polarity", "tweet"])
sentiment_df = sentiment_df[sentiment_df.polarity != 0]
fig, ax = plt.subplots(figsize=(8, 6))
# Plot histogram of the polarity values
sentiment_df.hist(bins=[-1, -0.75, -0.5, -0.25, 0.25, 0.5, 0.75, 1],
             ax=ax,
             color="purple")

plt.title("Sentiments from Tweets")
plt.show()

